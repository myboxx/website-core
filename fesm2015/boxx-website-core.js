import { __decorate, __param } from 'tslib';
import { HttpParams, HttpClient, HttpEventType, HttpClientModule } from '@angular/common/http';
import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

const WEBSITE_REPOSITORY = new InjectionToken('websiteRepository');

let WebsiteRepository = class WebsiteRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    deleteImage(payload /*website_id: string, container: string, index: string, url_img: string*/) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/delete_image`, body);
    }
    getWebsiteData() {
        return this.httpClient.get(`${this.getBaseUrl()}/sites`);
    }
    updateWebsiteData(siteId, payload) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/update_site/${siteId}`, body);
    }
    updateBusinessData(siteId, payload) {
        const urlSearchParams = new URLSearchParams();
        Object.keys(payload).forEach((key) => {
            (typeof payload[key] === 'object') ?
                urlSearchParams.append(key, JSON.stringify(payload[key]))
                :
                    urlSearchParams.append(key, payload[key]);
        });
        const body = urlSearchParams.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/update_business_info/${siteId}`, body);
    }
    getUploadImagesUrl() {
        return this.getBaseUrl() + '/upload_update_images';
    }
    uploadImage(fileToUpload, config) {
        const formData = new FormData();
        formData.append('img', fileToUpload, fileToUpload.name.replace('jpeg', 'jpg'));
        Object.keys(config).forEach(key => {
            formData.append(key, config[key]);
        });
        return this.httpClient.post(`${this.getUploadImagesUrl()}`, formData, {
            reportProgress: true,
            observe: 'events'
        });
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/website`;
    }
};
WebsiteRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
WebsiteRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], WebsiteRepository);

const WEBSITE_SERVICE = new InjectionToken('websiteService');

class WebsitePageModel {
    constructor(data) {
        this.websiteList = data.websiteList;
    }
    static fromApiResponse(raw) {
        const websiteList = raw.map(resp => {
            return WebsiteModel.fromApiResponse(resp);
        });
        return new WebsitePageModel({
            websiteList
        });
    }
    static empty() {
        return new WebsitePageModel({
            websiteList: [WebsiteModel.empty()]
        });
    }
}
class WebsiteModel {
    constructor(data) {
        this.siteId = data.siteId;
        this.title = data.title;
        this.email = data.email;
        this.facebook = data.facebook && data.facebook !== 'null' ? data.facebook : '';
        this.twitter = data.twitter && data.twitter !== 'null' ? data.twitter : '';
        this.instagram = data.instagram && data.instagram !== 'null' ? data.instagram : '';
        this.gplus = data.gplus && data.gplus !== 'null' ? data.gplus : '';
        this.siteUrl = data.siteUrl;
        this.subdomain = data.subdomain;
        this.about = data.about;
        this.displayPicture = data.displayPicture;
        this.phone = data.phone;
        this.mobile = data.mobile;
        this.hoursOfOperation = data.hoursOfOperation || [];
        this.hoursOfOperationNotes = data.hoursOfOperationNotes || '';
        this.paymentForms = data.paymentForms || [];
        this.products = data.products || [];
    }
    static fromApiResponse(data) {
        let siteUrl = (data.site_data.site_url || '').replace('https', 'http');
        if (siteUrl) {
            siteUrl = siteUrl.startsWith('http') ? siteUrl : 'http://' + siteUrl;
        }
        return new WebsiteModel({
            siteId: +data.site_id,
            title: data.site_data.title,
            email: data.site_data.email,
            facebook: data.site_data.facebook,
            twitter: data.site_data.twitter,
            instagram: data.site_data.instagram,
            gplus: data.site_data.gplus,
            siteUrl,
            subdomain: data.site_data.subdomian,
            about: data.site_data.about,
            displayPicture: {
                carrouselUrlImages: data.site_data.display_picture.carrousel_url_images,
                logoUrlImage: data.site_data.display_picture.logo_url_image,
                bannerUrl: data.site_data.tagline
            },
            phone: data.site_data.phone,
            mobile: data.site_data.mobile,
            hoursOfOperation: data.site_data.hours_of_operation,
            hoursOfOperationNotes: data.site_data.hours_of_operation_notes,
            paymentForms: data.site_data.payment_forms,
            products: data.site_data.products
        });
    }
    static empty() {
        return new WebsiteModel({
            siteId: null,
            title: null,
            email: null,
            facebook: null,
            twitter: null,
            instagram: null,
            gplus: null,
            siteUrl: null,
            subdomain: null,
            about: null,
            displayPicture: null,
            phone: null,
            mobile: null,
            hoursOfOperation: null,
            hoursOfOperationNotes: null,
            paymentForms: null,
            products: null
        });
    }
}

let WebsiteService = class WebsiteService {
    constructor(repository) {
        this.repository = repository;
    }
    deleteImage(payload) {
        return this.repository.deleteImage(payload).pipe(map((response) => {
            return response.data;
        }), catchError(error => {
            throw error;
        }));
    }
    getWebsiteData() {
        return this.repository.getWebsiteData().pipe(map((response) => {
            return WebsitePageModel.fromApiResponse(response.data);
        }), catchError(error => {
            throw error;
        }));
    }
    updateWebsiteData(siteId, payload) {
        return this.repository.updateWebsiteData(siteId, payload).pipe(map((response) => {
            return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
        }), catchError(error => {
            throw error;
        }));
    }
    updateBusinessData(siteId, payload) {
        return this.repository.updateBusinessData(siteId, payload).pipe(map((response) => {
            return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
        }), catchError(error => {
            throw error;
        }));
    }
    uploadFileWebBrowser(file, params, onProgressEvent) {
        return new Promise((resolve, reject) => {
            this.repository.uploadImage(file, params)
                .subscribe((event) => {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        if (onProgressEvent) {
                            onProgressEvent(event);
                        }
                        break;
                    case HttpEventType.Response:
                        resolve(true);
                }
            }, error => reject(error));
        });
    }
};
WebsiteService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [WEBSITE_REPOSITORY,] }] }
];
WebsiteService.ɵprov = ɵɵdefineInjectable({ factory: function WebsiteService_Factory() { return new WebsiteService(ɵɵinject(WEBSITE_REPOSITORY)); }, token: WebsiteService, providedIn: "root" });
WebsiteService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(WEBSITE_REPOSITORY))
], WebsiteService);

var WebsiteActionTypes;
(function (WebsiteActionTypes) {
    WebsiteActionTypes["LoadWebsiteBegin"] = "[WEBSITE] Load Webiste Begin";
    WebsiteActionTypes["LoadWebsiteSuccess"] = "[WEBSITE] Load Webiste Success";
    WebsiteActionTypes["LoadWebsiteFail"] = "[WEBSITE] Load Webiste Fail";
    WebsiteActionTypes["UpdateWebsiteBegin"] = "[WEBSITE] Update Webiste Begin";
    WebsiteActionTypes["UpdateWebsiteSuccess"] = "[WEBSITE] Update Webiste Success";
    WebsiteActionTypes["UpdateWebsiteFail"] = "[WEBSITE] Update Webiste Fail";
    WebsiteActionTypes["UpdateBusinessBegin"] = "[WEBSITE] Update Business Begin";
    WebsiteActionTypes["UpdateBusinessSuccess"] = "[WEBSITE] Update Business Success";
    WebsiteActionTypes["UpdateBusinessFail"] = "[WEBSITE] Update Business Fail";
    WebsiteActionTypes["DeleteImageBegin"] = "[WEBSITE] Delete Image Begin";
    WebsiteActionTypes["DeleteImageSuccess"] = "[WEBSITE] Delete Image Success";
    WebsiteActionTypes["DeleteImageFail"] = "[WEBSITE] Delete Image Fail";
})(WebsiteActionTypes || (WebsiteActionTypes = {}));
const LoadWebsiteBeginAction = createAction(WebsiteActionTypes.LoadWebsiteBegin);
const LoadWebsiteSuccessAction = createAction(WebsiteActionTypes.LoadWebsiteSuccess, props());
const LoadWebsiteFailAction = createAction(WebsiteActionTypes.LoadWebsiteFail, props());
const UpdateWebsiteBeginAction = createAction(WebsiteActionTypes.UpdateWebsiteBegin, props());
const UpdateWebsiteSuccessAction = createAction(WebsiteActionTypes.UpdateWebsiteSuccess, props());
const UpdateWebsiteFailAction = createAction(WebsiteActionTypes.UpdateWebsiteFail, props());
const UpdateBusinessBeginAction = createAction(WebsiteActionTypes.UpdateBusinessBegin, props());
const UpdateBusinessSuccessAction = createAction(WebsiteActionTypes.UpdateBusinessSuccess, props());
const UpdateBusinessFailAction = createAction(WebsiteActionTypes.UpdateBusinessFail, props());
// -------------------- Image section -----------------
const DeleteImageBeginAction = createAction(WebsiteActionTypes.DeleteImageBegin, props());
const DeleteImageSuccessAction = createAction(WebsiteActionTypes.DeleteImageSuccess, props());
const DeleteImageFailAction = createAction(WebsiteActionTypes.DeleteImageFail, props());

let WebsiteEffects = class WebsiteEffects {
    constructor(actions$, service) {
        this.actions$ = actions$;
        this.service = service;
        this.loadWebsite$ = createEffect(() => this.actions$.pipe(ofType(WebsiteActionTypes.LoadWebsiteBegin), switchMap(() => {
            return this.service.getWebsiteData().pipe(map(websitePage => LoadWebsiteSuccessAction({ payload: websitePage })), catchError(errors => {
                console.error('WebsiteEffects.loadWebsite$ ERROR', errors);
                return of(LoadWebsiteFailAction({ errors }));
            }));
        })));
        this.updateWebsite$ = createEffect(() => this.actions$.pipe(ofType(WebsiteActionTypes.UpdateWebsiteBegin), switchMap((action) => {
            return this.service.updateWebsiteData(action.siteId, action.payload).pipe(map(response => {
                return UpdateWebsiteSuccessAction({ payload: response });
            }), catchError(errors => {
                console.error('WebsiteEffects.updateWebsite$ ERROR', errors);
                return of(UpdateWebsiteFailAction({ errors }));
            }));
        })));
        this.updateBusiness$ = createEffect(() => this.actions$.pipe(ofType(WebsiteActionTypes.UpdateBusinessBegin), switchMap((action) => {
            return this.service.updateBusinessData(action.siteId, action.payload).pipe(map(response => {
                return UpdateBusinessSuccessAction({ payload: response });
            }), catchError(errors => {
                console.error('WebsiteEffects.updateBusiness$ ERROR', errors);
                return of(UpdateBusinessFailAction({ errors }));
            }));
        })));
        this.deleteImage$ = createEffect(() => this.actions$.pipe(ofType(WebsiteActionTypes.DeleteImageBegin), switchMap((action) => {
            return this.service.deleteImage(action.payload).pipe(map(response => {
                return DeleteImageSuccessAction({ response });
            }), catchError(errors => {
                console.error('WebsiteEffects.deleteImage$ ERROR', errors);
                return of(DeleteImageFailAction({ errors }));
            }));
        })));
    }
};
WebsiteEffects.ctorParameters = () => [
    { type: Actions },
    { type: undefined, decorators: [{ type: Inject, args: [WEBSITE_SERVICE,] }] }
];
WebsiteEffects = __decorate([
    Injectable(),
    __param(1, Inject(WEBSITE_SERVICE))
], WebsiteEffects);

const ɵ0 = WebsitePageModel.empty();
const initialState = {
    isLoading: false,
    isLoadingImages: false,
    data: ɵ0,
    hasBeenFetched: false,
    error: null,
    success: null
};
const ɵ1 = (state, action) => (Object.assign(Object.assign({}, state), { error: null, success: null, isLoading: action.type !== WebsiteActionTypes.DeleteImageBegin ? true : false, isLoadingImages: action.type === WebsiteActionTypes.DeleteImageBegin ? true : false })), ɵ2 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, isLoadingImages: false, error: { after: getErrorActionType(action.type), error: action.errors } })), ɵ3 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, hasBeenFetched: true, data: action.payload, success: { after: getSuccessActionType(action.type) } })), ɵ4 = (state, action) => (Object.assign(Object.assign({}, state), { isLoadingImages: false, success: { after: getSuccessActionType(action.type) } })), ɵ5 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, data: action.payload, success: { after: getSuccessActionType(action.type) } }));
const reducer = createReducer(initialState, 
// LOAD SECTION
on(LoadWebsiteBeginAction, UpdateWebsiteBeginAction, UpdateBusinessBeginAction, DeleteImageBeginAction, ɵ1), on(LoadWebsiteFailAction, UpdateWebsiteFailAction, UpdateBusinessFailAction, DeleteImageFailAction, ɵ2), on(LoadWebsiteSuccessAction, ɵ3), on(DeleteImageSuccessAction, ɵ4), on(UpdateWebsiteSuccessAction, UpdateBusinessSuccessAction, ɵ5));
function getErrorActionType(type) {
    let action;
    switch (type) {
        case WebsiteActionTypes.LoadWebsiteFail:
            action = 'GET';
            break;
        case WebsiteActionTypes.UpdateWebsiteFail:
        case WebsiteActionTypes.UpdateBusinessFail:
            action = 'UPDATE';
            break;
        case WebsiteActionTypes.DeleteImageFail:
            action = 'DELETE_IMAGE';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    let action;
    switch (type) {
        case WebsiteActionTypes.LoadWebsiteSuccess:
            action = 'GET';
            break;
        case WebsiteActionTypes.UpdateWebsiteSuccess:
        case WebsiteActionTypes.UpdateBusinessSuccess:
            action = 'UPDATE';
            break;
        case WebsiteActionTypes.DeleteImageSuccess:
            action = 'DELETE_IMAGE';
            break;
    }
    return action;
}
function websiteReducer(state, action) {
    return reducer(state, action);
}

const getWebsiteState = createFeatureSelector('website');
const ɵ0$1 = state => state;
const getWebsitePageState = createSelector(getWebsiteState, ɵ0$1);
const stateGetIsLoading = (state) => state.isLoading;
const stateGetIsLoadingImages = (state) => state.isLoadingImages;
const stateGetWebsiteData = (state) => state.data;
const getIsLoading = createSelector(getWebsitePageState, stateGetIsLoading);
const getIsLoadingImages = createSelector(getWebsitePageState, stateGetIsLoadingImages);
const ɵ1$1 = state => state.error;
const getError = createSelector(getWebsitePageState, ɵ1$1);
const ɵ2$1 = state => state.success;
const getSuccess = createSelector(getWebsitePageState, ɵ2$1);
const getWebsiteData = createSelector(getWebsitePageState, stateGetWebsiteData);
const ɵ3$1 = state => state.hasBeenFetched;
const hasBeenFetched = createSelector(getWebsitePageState, ɵ3$1);

let WebsiteStore = class WebsiteStore {
    constructor(store) {
        this.store = store;
    }
    get Loading$() { return this.store.select(getIsLoading); }
    get ImagesLoading$() { return this.store.select(getIsLoadingImages); }
    get Error$() { return this.store.select(getError); }
    get Success$() { return this.store.select(getSuccess); }
    LoadWebsite() { this.store.dispatch(LoadWebsiteBeginAction()); }
    get Website$() { return this.store.select(getWebsiteData); }
    get HasBeenFetched$() {
        return this.store.select(hasBeenFetched);
    }
    UpdateWebsite(siteId, payload) {
        this.store.dispatch(UpdateWebsiteBeginAction({ siteId, payload }));
    }
    UpdateBusinessData(siteId, payload) {
        this.store.dispatch(UpdateBusinessBeginAction({ siteId, payload }));
    }
    deleteImage(payload) {
        this.store.dispatch(DeleteImageBeginAction({ payload }));
    }
};
WebsiteStore.ctorParameters = () => [
    { type: Store }
];
WebsiteStore = __decorate([
    Injectable()
], WebsiteStore);

var WebsiteCoreModule_1;
let WebsiteCoreModule = WebsiteCoreModule_1 = class WebsiteCoreModule {
    static forRoot(config) {
        return {
            ngModule: WebsiteCoreModule_1,
            providers: [
                { provide: WEBSITE_SERVICE, useClass: WebsiteService },
                { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository },
                ...config.providers,
                WebsiteStore
            ]
        };
    }
};
WebsiteCoreModule = WebsiteCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('website', websiteReducer),
            EffectsModule.forFeature([WebsiteEffects]),
        ],
        exports: []
    })
], WebsiteCoreModule);

/*
 * Public API Surface of website-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DeleteImageBeginAction, DeleteImageFailAction, DeleteImageSuccessAction, LoadWebsiteBeginAction, LoadWebsiteFailAction, LoadWebsiteSuccessAction, UpdateBusinessBeginAction, UpdateBusinessFailAction, UpdateBusinessSuccessAction, UpdateWebsiteBeginAction, UpdateWebsiteFailAction, UpdateWebsiteSuccessAction, WEBSITE_REPOSITORY, WEBSITE_SERVICE, WebsiteActionTypes, WebsiteCoreModule, WebsiteEffects, WebsiteModel, WebsitePageModel, WebsiteRepository, WebsiteService, WebsiteStore, getError, getIsLoading, getIsLoadingImages, getSuccess, getWebsiteData, getWebsitePageState, getWebsiteState, hasBeenFetched, initialState, stateGetIsLoading, stateGetIsLoadingImages, stateGetWebsiteData, websiteReducer, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3 };
//# sourceMappingURL=boxx-website-core.js.map
