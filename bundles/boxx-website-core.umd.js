(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common/http'), require('@angular/core'), require('@ngrx/effects'), require('@ngrx/store'), require('@boxx/core'), require('rxjs/operators'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define('@boxx/website-core', ['exports', '@angular/common/http', '@angular/core', '@ngrx/effects', '@ngrx/store', '@boxx/core', 'rxjs/operators', 'rxjs'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['website-core'] = {}), global.ng.common.http, global.ng.core, global['ngrx-effects'], global['ngrx-store'], global['boxx-core'], global.rxjs.operators, global.rxjs));
}(this, (function (exports, http, core, effects, store, core$1, operators, rxjs) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var WEBSITE_REPOSITORY = new core.InjectionToken('websiteRepository');

    var WebsiteRepository = /** @class */ (function () {
        function WebsiteRepository(appSettings, httpClient) {
            this.appSettings = appSettings;
            this.httpClient = httpClient;
        }
        WebsiteRepository.prototype.deleteImage = function (payload /*website_id: string, container: string, index: string, url_img: string*/) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    params = params.append(key, payload[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/delete_image", body);
        };
        WebsiteRepository.prototype.getWebsiteData = function () {
            return this.httpClient.get(this.getBaseUrl() + "/sites");
        };
        WebsiteRepository.prototype.updateWebsiteData = function (siteId, payload) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    params = params.append(key, payload[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/update_site/" + siteId, body);
        };
        WebsiteRepository.prototype.updateBusinessData = function (siteId, payload) {
            var urlSearchParams = new URLSearchParams();
            Object.keys(payload).forEach(function (key) {
                (typeof payload[key] === 'object') ?
                    urlSearchParams.append(key, JSON.stringify(payload[key]))
                    :
                        urlSearchParams.append(key, payload[key]);
            });
            var body = urlSearchParams.toString();
            return this.httpClient.post(this.getBaseUrl() + "/update_business_info/" + siteId, body);
        };
        WebsiteRepository.prototype.getUploadImagesUrl = function () {
            return this.getBaseUrl() + '/upload_update_images';
        };
        WebsiteRepository.prototype.uploadImage = function (fileToUpload, config) {
            var formData = new FormData();
            formData.append('img', fileToUpload, fileToUpload.name.replace('jpeg', 'jpg'));
            Object.keys(config).forEach(function (key) {
                formData.append(key, config[key]);
            });
            return this.httpClient.post("" + this.getUploadImagesUrl(), formData, {
                reportProgress: true,
                observe: 'events'
            });
        };
        WebsiteRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/website";
        };
        WebsiteRepository.ctorParameters = function () { return [
            { type: core$1.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$1.APP_CONFIG_SERVICE,] }] },
            { type: http.HttpClient }
        ]; };
        WebsiteRepository = __decorate([
            core.Injectable(),
            __param(0, core.Inject(core$1.APP_CONFIG_SERVICE))
        ], WebsiteRepository);
        return WebsiteRepository;
    }());

    var WEBSITE_SERVICE = new core.InjectionToken('websiteService');

    var WebsitePageModel = /** @class */ (function () {
        function WebsitePageModel(data) {
            this.websiteList = data.websiteList;
        }
        WebsitePageModel.fromApiResponse = function (raw) {
            var websiteList = raw.map(function (resp) {
                return WebsiteModel.fromApiResponse(resp);
            });
            return new WebsitePageModel({
                websiteList: websiteList
            });
        };
        WebsitePageModel.empty = function () {
            return new WebsitePageModel({
                websiteList: [WebsiteModel.empty()]
            });
        };
        return WebsitePageModel;
    }());
    var WebsiteModel = /** @class */ (function () {
        function WebsiteModel(data) {
            this.siteId = data.siteId;
            this.title = data.title;
            this.email = data.email;
            this.facebook = data.facebook && data.facebook !== 'null' ? data.facebook : '';
            this.twitter = data.twitter && data.twitter !== 'null' ? data.twitter : '';
            this.instagram = data.instagram && data.instagram !== 'null' ? data.instagram : '';
            this.gplus = data.gplus && data.gplus !== 'null' ? data.gplus : '';
            this.siteUrl = data.siteUrl;
            this.subdomain = data.subdomain;
            this.about = data.about;
            this.displayPicture = data.displayPicture;
            this.phone = data.phone;
            this.mobile = data.mobile;
            this.hoursOfOperation = data.hoursOfOperation || [];
            this.hoursOfOperationNotes = data.hoursOfOperationNotes || '';
            this.paymentForms = data.paymentForms || [];
            this.products = data.products || [];
        }
        WebsiteModel.fromApiResponse = function (data) {
            var siteUrl = (data.site_data.site_url || '').replace('https', 'http');
            if (siteUrl) {
                siteUrl = siteUrl.startsWith('http') ? siteUrl : 'http://' + siteUrl;
            }
            return new WebsiteModel({
                siteId: +data.site_id,
                title: data.site_data.title,
                email: data.site_data.email,
                facebook: data.site_data.facebook,
                twitter: data.site_data.twitter,
                instagram: data.site_data.instagram,
                gplus: data.site_data.gplus,
                siteUrl: siteUrl,
                subdomain: data.site_data.subdomian,
                about: data.site_data.about,
                displayPicture: {
                    carrouselUrlImages: data.site_data.display_picture.carrousel_url_images,
                    logoUrlImage: data.site_data.display_picture.logo_url_image,
                    bannerUrl: data.site_data.tagline
                },
                phone: data.site_data.phone,
                mobile: data.site_data.mobile,
                hoursOfOperation: data.site_data.hours_of_operation,
                hoursOfOperationNotes: data.site_data.hours_of_operation_notes,
                paymentForms: data.site_data.payment_forms,
                products: data.site_data.products
            });
        };
        WebsiteModel.empty = function () {
            return new WebsiteModel({
                siteId: null,
                title: null,
                email: null,
                facebook: null,
                twitter: null,
                instagram: null,
                gplus: null,
                siteUrl: null,
                subdomain: null,
                about: null,
                displayPicture: null,
                phone: null,
                mobile: null,
                hoursOfOperation: null,
                hoursOfOperationNotes: null,
                paymentForms: null,
                products: null
            });
        };
        return WebsiteModel;
    }());

    var WebsiteService = /** @class */ (function () {
        function WebsiteService(repository) {
            this.repository = repository;
        }
        WebsiteService.prototype.deleteImage = function (payload) {
            return this.repository.deleteImage(payload).pipe(operators.map(function (response) {
                return response.data;
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        WebsiteService.prototype.getWebsiteData = function () {
            return this.repository.getWebsiteData().pipe(operators.map(function (response) {
                return WebsitePageModel.fromApiResponse(response.data);
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        WebsiteService.prototype.updateWebsiteData = function (siteId, payload) {
            return this.repository.updateWebsiteData(siteId, payload).pipe(operators.map(function (response) {
                return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        WebsiteService.prototype.updateBusinessData = function (siteId, payload) {
            return this.repository.updateBusinessData(siteId, payload).pipe(operators.map(function (response) {
                return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        WebsiteService.prototype.uploadFileWebBrowser = function (file, params, onProgressEvent) {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _this.repository.uploadImage(file, params)
                    .subscribe(function (event) {
                    switch (event.type) {
                        case http.HttpEventType.UploadProgress:
                            if (onProgressEvent) {
                                onProgressEvent(event);
                            }
                            break;
                        case http.HttpEventType.Response:
                            resolve(true);
                    }
                }, function (error) { return reject(error); });
            });
        };
        WebsiteService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [WEBSITE_REPOSITORY,] }] }
        ]; };
        WebsiteService.ɵprov = core.ɵɵdefineInjectable({ factory: function WebsiteService_Factory() { return new WebsiteService(core.ɵɵinject(WEBSITE_REPOSITORY)); }, token: WebsiteService, providedIn: "root" });
        WebsiteService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(WEBSITE_REPOSITORY))
        ], WebsiteService);
        return WebsiteService;
    }());


    (function (WebsiteActionTypes) {
        WebsiteActionTypes["LoadWebsiteBegin"] = "[WEBSITE] Load Webiste Begin";
        WebsiteActionTypes["LoadWebsiteSuccess"] = "[WEBSITE] Load Webiste Success";
        WebsiteActionTypes["LoadWebsiteFail"] = "[WEBSITE] Load Webiste Fail";
        WebsiteActionTypes["UpdateWebsiteBegin"] = "[WEBSITE] Update Webiste Begin";
        WebsiteActionTypes["UpdateWebsiteSuccess"] = "[WEBSITE] Update Webiste Success";
        WebsiteActionTypes["UpdateWebsiteFail"] = "[WEBSITE] Update Webiste Fail";
        WebsiteActionTypes["UpdateBusinessBegin"] = "[WEBSITE] Update Business Begin";
        WebsiteActionTypes["UpdateBusinessSuccess"] = "[WEBSITE] Update Business Success";
        WebsiteActionTypes["UpdateBusinessFail"] = "[WEBSITE] Update Business Fail";
        WebsiteActionTypes["DeleteImageBegin"] = "[WEBSITE] Delete Image Begin";
        WebsiteActionTypes["DeleteImageSuccess"] = "[WEBSITE] Delete Image Success";
        WebsiteActionTypes["DeleteImageFail"] = "[WEBSITE] Delete Image Fail";
    })(exports.WebsiteActionTypes || (exports.WebsiteActionTypes = {}));
    var LoadWebsiteBeginAction = store.createAction(exports.WebsiteActionTypes.LoadWebsiteBegin);
    var LoadWebsiteSuccessAction = store.createAction(exports.WebsiteActionTypes.LoadWebsiteSuccess, store.props());
    var LoadWebsiteFailAction = store.createAction(exports.WebsiteActionTypes.LoadWebsiteFail, store.props());
    var UpdateWebsiteBeginAction = store.createAction(exports.WebsiteActionTypes.UpdateWebsiteBegin, store.props());
    var UpdateWebsiteSuccessAction = store.createAction(exports.WebsiteActionTypes.UpdateWebsiteSuccess, store.props());
    var UpdateWebsiteFailAction = store.createAction(exports.WebsiteActionTypes.UpdateWebsiteFail, store.props());
    var UpdateBusinessBeginAction = store.createAction(exports.WebsiteActionTypes.UpdateBusinessBegin, store.props());
    var UpdateBusinessSuccessAction = store.createAction(exports.WebsiteActionTypes.UpdateBusinessSuccess, store.props());
    var UpdateBusinessFailAction = store.createAction(exports.WebsiteActionTypes.UpdateBusinessFail, store.props());
    // -------------------- Image section -----------------
    var DeleteImageBeginAction = store.createAction(exports.WebsiteActionTypes.DeleteImageBegin, store.props());
    var DeleteImageSuccessAction = store.createAction(exports.WebsiteActionTypes.DeleteImageSuccess, store.props());
    var DeleteImageFailAction = store.createAction(exports.WebsiteActionTypes.DeleteImageFail, store.props());

    var WebsiteEffects = /** @class */ (function () {
        function WebsiteEffects(actions$, service) {
            var _this = this;
            this.actions$ = actions$;
            this.service = service;
            this.loadWebsite$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.WebsiteActionTypes.LoadWebsiteBegin), operators.switchMap(function () {
                return _this.service.getWebsiteData().pipe(operators.map(function (websitePage) { return LoadWebsiteSuccessAction({ payload: websitePage }); }), operators.catchError(function (errors) {
                    console.error('WebsiteEffects.loadWebsite$ ERROR', errors);
                    return rxjs.of(LoadWebsiteFailAction({ errors: errors }));
                }));
            })); });
            this.updateWebsite$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.WebsiteActionTypes.UpdateWebsiteBegin), operators.switchMap(function (action) {
                return _this.service.updateWebsiteData(action.siteId, action.payload).pipe(operators.map(function (response) {
                    return UpdateWebsiteSuccessAction({ payload: response });
                }), operators.catchError(function (errors) {
                    console.error('WebsiteEffects.updateWebsite$ ERROR', errors);
                    return rxjs.of(UpdateWebsiteFailAction({ errors: errors }));
                }));
            })); });
            this.updateBusiness$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.WebsiteActionTypes.UpdateBusinessBegin), operators.switchMap(function (action) {
                return _this.service.updateBusinessData(action.siteId, action.payload).pipe(operators.map(function (response) {
                    return UpdateBusinessSuccessAction({ payload: response });
                }), operators.catchError(function (errors) {
                    console.error('WebsiteEffects.updateBusiness$ ERROR', errors);
                    return rxjs.of(UpdateBusinessFailAction({ errors: errors }));
                }));
            })); });
            this.deleteImage$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.WebsiteActionTypes.DeleteImageBegin), operators.switchMap(function (action) {
                return _this.service.deleteImage(action.payload).pipe(operators.map(function (response) {
                    return DeleteImageSuccessAction({ response: response });
                }), operators.catchError(function (errors) {
                    console.error('WebsiteEffects.deleteImage$ ERROR', errors);
                    return rxjs.of(DeleteImageFailAction({ errors: errors }));
                }));
            })); });
        }
        WebsiteEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: undefined, decorators: [{ type: core.Inject, args: [WEBSITE_SERVICE,] }] }
        ]; };
        WebsiteEffects = __decorate([
            core.Injectable(),
            __param(1, core.Inject(WEBSITE_SERVICE))
        ], WebsiteEffects);
        return WebsiteEffects;
    }());

    var ɵ0 = WebsitePageModel.empty();
    var initialState = {
        isLoading: false,
        isLoadingImages: false,
        data: ɵ0,
        hasBeenFetched: false,
        error: null,
        success: null
    };
    var ɵ1 = function (state, action) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: action.type !== exports.WebsiteActionTypes.DeleteImageBegin ? true : false, isLoadingImages: action.type === exports.WebsiteActionTypes.DeleteImageBegin ? true : false })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, isLoadingImages: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, hasBeenFetched: true, data: action.payload, success: { after: getSuccessActionType(action.type) } })); }, ɵ4 = function (state, action) { return (__assign(__assign({}, state), { isLoadingImages: false, success: { after: getSuccessActionType(action.type) } })); }, ɵ5 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, data: action.payload, success: { after: getSuccessActionType(action.type) } })); };
    var reducer = store.createReducer(initialState, 
    // LOAD SECTION
    store.on(LoadWebsiteBeginAction, UpdateWebsiteBeginAction, UpdateBusinessBeginAction, DeleteImageBeginAction, ɵ1), store.on(LoadWebsiteFailAction, UpdateWebsiteFailAction, UpdateBusinessFailAction, DeleteImageFailAction, ɵ2), store.on(LoadWebsiteSuccessAction, ɵ3), store.on(DeleteImageSuccessAction, ɵ4), store.on(UpdateWebsiteSuccessAction, UpdateBusinessSuccessAction, ɵ5));
    function getErrorActionType(type) {
        var action;
        switch (type) {
            case exports.WebsiteActionTypes.LoadWebsiteFail:
                action = 'GET';
                break;
            case exports.WebsiteActionTypes.UpdateWebsiteFail:
            case exports.WebsiteActionTypes.UpdateBusinessFail:
                action = 'UPDATE';
                break;
            case exports.WebsiteActionTypes.DeleteImageFail:
                action = 'DELETE_IMAGE';
                break;
        }
        return action;
    }
    function getSuccessActionType(type) {
        var action;
        switch (type) {
            case exports.WebsiteActionTypes.LoadWebsiteSuccess:
                action = 'GET';
                break;
            case exports.WebsiteActionTypes.UpdateWebsiteSuccess:
            case exports.WebsiteActionTypes.UpdateBusinessSuccess:
                action = 'UPDATE';
                break;
            case exports.WebsiteActionTypes.DeleteImageSuccess:
                action = 'DELETE_IMAGE';
                break;
        }
        return action;
    }
    function websiteReducer(state, action) {
        return reducer(state, action);
    }

    var getWebsiteState = store.createFeatureSelector('website');
    var ɵ0$1 = function (state) { return state; };
    var getWebsitePageState = store.createSelector(getWebsiteState, ɵ0$1);
    var stateGetIsLoading = function (state) { return state.isLoading; };
    var stateGetIsLoadingImages = function (state) { return state.isLoadingImages; };
    var stateGetWebsiteData = function (state) { return state.data; };
    var getIsLoading = store.createSelector(getWebsitePageState, stateGetIsLoading);
    var getIsLoadingImages = store.createSelector(getWebsitePageState, stateGetIsLoadingImages);
    var ɵ1$1 = function (state) { return state.error; };
    var getError = store.createSelector(getWebsitePageState, ɵ1$1);
    var ɵ2$1 = function (state) { return state.success; };
    var getSuccess = store.createSelector(getWebsitePageState, ɵ2$1);
    var getWebsiteData = store.createSelector(getWebsitePageState, stateGetWebsiteData);
    var ɵ3$1 = function (state) { return state.hasBeenFetched; };
    var hasBeenFetched = store.createSelector(getWebsitePageState, ɵ3$1);

    var WebsiteStore = /** @class */ (function () {
        function WebsiteStore(store) {
            this.store = store;
        }
        Object.defineProperty(WebsiteStore.prototype, "Loading$", {
            get: function () { return this.store.select(getIsLoading); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebsiteStore.prototype, "ImagesLoading$", {
            get: function () { return this.store.select(getIsLoadingImages); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebsiteStore.prototype, "Error$", {
            get: function () { return this.store.select(getError); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebsiteStore.prototype, "Success$", {
            get: function () { return this.store.select(getSuccess); },
            enumerable: true,
            configurable: true
        });
        WebsiteStore.prototype.LoadWebsite = function () { this.store.dispatch(LoadWebsiteBeginAction()); };
        Object.defineProperty(WebsiteStore.prototype, "Website$", {
            get: function () { return this.store.select(getWebsiteData); },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WebsiteStore.prototype, "HasBeenFetched$", {
            get: function () {
                return this.store.select(hasBeenFetched);
            },
            enumerable: true,
            configurable: true
        });
        WebsiteStore.prototype.UpdateWebsite = function (siteId, payload) {
            this.store.dispatch(UpdateWebsiteBeginAction({ siteId: siteId, payload: payload }));
        };
        WebsiteStore.prototype.UpdateBusinessData = function (siteId, payload) {
            this.store.dispatch(UpdateBusinessBeginAction({ siteId: siteId, payload: payload }));
        };
        WebsiteStore.prototype.deleteImage = function (payload) {
            this.store.dispatch(DeleteImageBeginAction({ payload: payload }));
        };
        WebsiteStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        WebsiteStore = __decorate([
            core.Injectable()
        ], WebsiteStore);
        return WebsiteStore;
    }());

    var WebsiteCoreModule = /** @class */ (function () {
        function WebsiteCoreModule() {
        }
        WebsiteCoreModule_1 = WebsiteCoreModule;
        WebsiteCoreModule.forRoot = function (config) {
            return {
                ngModule: WebsiteCoreModule_1,
                providers: __spread([
                    { provide: WEBSITE_SERVICE, useClass: WebsiteService },
                    { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository }
                ], config.providers, [
                    WebsiteStore
                ])
            };
        };
        var WebsiteCoreModule_1;
        WebsiteCoreModule = WebsiteCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('website', websiteReducer),
                    effects.EffectsModule.forFeature([WebsiteEffects]),
                ],
                exports: []
            })
        ], WebsiteCoreModule);
        return WebsiteCoreModule;
    }());

    exports.DeleteImageBeginAction = DeleteImageBeginAction;
    exports.DeleteImageFailAction = DeleteImageFailAction;
    exports.DeleteImageSuccessAction = DeleteImageSuccessAction;
    exports.LoadWebsiteBeginAction = LoadWebsiteBeginAction;
    exports.LoadWebsiteFailAction = LoadWebsiteFailAction;
    exports.LoadWebsiteSuccessAction = LoadWebsiteSuccessAction;
    exports.UpdateBusinessBeginAction = UpdateBusinessBeginAction;
    exports.UpdateBusinessFailAction = UpdateBusinessFailAction;
    exports.UpdateBusinessSuccessAction = UpdateBusinessSuccessAction;
    exports.UpdateWebsiteBeginAction = UpdateWebsiteBeginAction;
    exports.UpdateWebsiteFailAction = UpdateWebsiteFailAction;
    exports.UpdateWebsiteSuccessAction = UpdateWebsiteSuccessAction;
    exports.WEBSITE_REPOSITORY = WEBSITE_REPOSITORY;
    exports.WEBSITE_SERVICE = WEBSITE_SERVICE;
    exports.WebsiteCoreModule = WebsiteCoreModule;
    exports.WebsiteEffects = WebsiteEffects;
    exports.WebsiteModel = WebsiteModel;
    exports.WebsitePageModel = WebsitePageModel;
    exports.WebsiteRepository = WebsiteRepository;
    exports.WebsiteService = WebsiteService;
    exports.WebsiteStore = WebsiteStore;
    exports.getError = getError;
    exports.getIsLoading = getIsLoading;
    exports.getIsLoadingImages = getIsLoadingImages;
    exports.getSuccess = getSuccess;
    exports.getWebsiteData = getWebsiteData;
    exports.getWebsitePageState = getWebsitePageState;
    exports.getWebsiteState = getWebsiteState;
    exports.hasBeenFetched = hasBeenFetched;
    exports.initialState = initialState;
    exports.stateGetIsLoading = stateGetIsLoading;
    exports.stateGetIsLoadingImages = stateGetIsLoadingImages;
    exports.stateGetWebsiteData = stateGetWebsiteData;
    exports.websiteReducer = websiteReducer;
    exports.ɵ0 = ɵ0$1;
    exports.ɵ1 = ɵ1$1;
    exports.ɵ2 = ɵ2$1;
    exports.ɵ3 = ɵ3$1;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-website-core.umd.js.map
