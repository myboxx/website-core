import { IWebsiteApiProps } from '../repositories/IWebsite.repository';
export declare class WebsitePageModel implements IWebsitePageProps {
    websiteList: WebsiteModel[];
    constructor(data: IWebsitePageProps);
    static fromApiResponse(raw: Array<IWebsiteApiProps>): IWebsitePageProps;
    static empty(): WebsitePageModel;
}
export declare class WebsiteModel implements IWebsiteModelProps {
    siteId: number;
    title: string;
    email: string;
    facebook: string;
    twitter: string;
    instagram: string;
    gplus: string;
    siteUrl: string;
    subdomain: string;
    about: string;
    displayPicture?: IWebsitePicturesProps;
    phone: string;
    mobile: string;
    hoursOfOperation: any[];
    hoursOfOperationNotes: string;
    paymentForms: ('visa' | 'mastercard' | 'cash' | 'checks' | 'american-express')[];
    products: string[];
    constructor(data: IWebsiteModelProps);
    static fromApiResponse(data: IWebsiteApiProps): IWebsiteModelProps;
    static empty(): WebsiteModel;
}
export interface IWebsiteModelProps {
    siteId: number;
    title: string;
    email: string;
    facebook: string;
    twitter: string;
    instagram: string;
    gplus: string;
    siteUrl: string;
    subdomain: string;
    about: string;
    displayPicture?: IWebsitePicturesProps;
    phone: string;
    mobile: string;
    hoursOfOperation: Array<any>;
    hoursOfOperationNotes: string;
    paymentForms: Array<'visa' | 'mastercard' | 'cash' | 'checks' | 'american-express'>;
    products: Array<string>;
}
export interface IWebsitePicturesProps {
    logoUrlImage: string;
    bannerUrl: string;
    carrouselUrlImages: {
        [key: number]: string;
    };
}
export interface IWebsitePageProps {
    websiteList: Array<WebsiteModel>;
}
