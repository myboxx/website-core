import { Observable } from 'rxjs';
import { WebsitePageModel } from '../models/website.model';
import { IBusinessFormProps, IDeleteImageFormProps, IWebsiteFormProps, IWebsiteRepository } from '../repositories/IWebsite.repository';
import { IWebsiteService } from './IWebsite.service';
export declare class WebsiteService implements IWebsiteService {
    private repository;
    constructor(repository: IWebsiteRepository);
    deleteImage(payload: IDeleteImageFormProps): Observable<import("../repositories/IWebsite.repository").IDeleteImageResponse>;
    getWebsiteData(): Observable<WebsitePageModel>;
    updateWebsiteData(siteId: number, payload: IWebsiteFormProps): Observable<WebsitePageModel>;
    updateBusinessData(siteId: number, payload: IBusinessFormProps): Observable<WebsitePageModel>;
    uploadFileWebBrowser(file: File, params: any, onProgressEvent: (event: any) => void): Promise<boolean>;
}
