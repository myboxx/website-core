import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { WebsitePageModel } from '../models/website.model';
import { IBusinessFormProps, IWebsiteFormProps } from '../repositories/IWebsite.repository';
export interface IWebsiteService {
    getWebsiteData(): Observable<WebsitePageModel>;
    updateWebsiteData(siteId: number, payload: IWebsiteFormProps): Observable<WebsitePageModel>;
    updateBusinessData(siteId: number, payload: IBusinessFormProps): Observable<WebsitePageModel>;
    deleteImage(payload: any): Observable<any>;
    uploadFileWebBrowser(file: File, params: any, onProgressEvent?: (event: any) => void): Promise<any>;
}
export declare const WEBSITE_SERVICE: InjectionToken<IWebsiteService>;
