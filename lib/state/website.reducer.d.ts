import { Action } from '@ngrx/store';
import { IWebsiteStateError, IWebsiteStateSuccess } from '../core/IStateErrorSuccess';
import { WebsitePageModel } from '../models/website.model';
export interface WebsiteState {
    isLoading: boolean;
    isLoadingImages: boolean;
    data: WebsitePageModel;
    hasBeenFetched: boolean;
    error: IWebsiteStateError;
    success: IWebsiteStateSuccess;
}
export declare const initialState: WebsiteState;
export declare function websiteReducer(state: WebsiteState | undefined, action: Action): WebsiteState;
export interface AppState {
    website: WebsiteState;
}
