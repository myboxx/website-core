import { Actions } from '@ngrx/effects';
import * as fromActions from './website.actions';
import { IWebsiteService } from '../services/IWebsite.service';
export declare class WebsiteEffects {
    private actions$;
    private service;
    loadWebsite$: import("rxjs").Observable<({
        payload: import("../models/website.model").WebsitePageModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.LoadWebsiteSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.LoadWebsiteFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    updateWebsite$: import("rxjs").Observable<({
        payload: import("../models/website.model").WebsitePageModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.UpdateWebsiteSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.UpdateWebsiteFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    updateBusiness$: import("rxjs").Observable<({
        payload: import("../models/website.model").WebsitePageModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.UpdateBusinessSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.UpdateBusinessFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    deleteImage$: import("rxjs").Observable<({
        response: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.DeleteImageSuccess>) | ({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.WebsiteActionTypes.DeleteImageFail>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, service: IWebsiteService);
}
