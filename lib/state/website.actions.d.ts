import { WebsitePageModel } from '../models/website.model';
import { IBusinessFormProps, IDeleteImageFormProps, IWebsiteFormProps } from '../repositories/IWebsite.repository';
export declare enum WebsiteActionTypes {
    LoadWebsiteBegin = "[WEBSITE] Load Webiste Begin",
    LoadWebsiteSuccess = "[WEBSITE] Load Webiste Success",
    LoadWebsiteFail = "[WEBSITE] Load Webiste Fail",
    UpdateWebsiteBegin = "[WEBSITE] Update Webiste Begin",
    UpdateWebsiteSuccess = "[WEBSITE] Update Webiste Success",
    UpdateWebsiteFail = "[WEBSITE] Update Webiste Fail",
    UpdateBusinessBegin = "[WEBSITE] Update Business Begin",
    UpdateBusinessSuccess = "[WEBSITE] Update Business Success",
    UpdateBusinessFail = "[WEBSITE] Update Business Fail",
    DeleteImageBegin = "[WEBSITE] Delete Image Begin",
    DeleteImageSuccess = "[WEBSITE] Delete Image Success",
    DeleteImageFail = "[WEBSITE] Delete Image Fail"
}
export declare const LoadWebsiteBeginAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.LoadWebsiteBegin, () => import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.LoadWebsiteBegin>>;
export declare const LoadWebsiteSuccessAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.LoadWebsiteSuccess, (props: {
    payload: WebsitePageModel;
}) => {
    payload: WebsitePageModel;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.LoadWebsiteSuccess>>;
export declare const LoadWebsiteFailAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.LoadWebsiteFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.LoadWebsiteFail>>;
export declare const UpdateWebsiteBeginAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.UpdateWebsiteBegin, (props: {
    siteId: number;
    payload: IWebsiteFormProps;
}) => {
    siteId: number;
    payload: IWebsiteFormProps;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.UpdateWebsiteBegin>>;
export declare const UpdateWebsiteSuccessAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.UpdateWebsiteSuccess, (props: {
    payload: WebsitePageModel;
}) => {
    payload: WebsitePageModel;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.UpdateWebsiteSuccess>>;
export declare const UpdateWebsiteFailAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.UpdateWebsiteFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.UpdateWebsiteFail>>;
export declare const UpdateBusinessBeginAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.UpdateBusinessBegin, (props: {
    siteId: number;
    payload: IBusinessFormProps;
}) => {
    siteId: number;
    payload: IBusinessFormProps;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.UpdateBusinessBegin>>;
export declare const UpdateBusinessSuccessAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.UpdateBusinessSuccess, (props: {
    payload: WebsitePageModel;
}) => {
    payload: WebsitePageModel;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.UpdateBusinessSuccess>>;
export declare const UpdateBusinessFailAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.UpdateBusinessFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.UpdateBusinessFail>>;
export declare const DeleteImageBeginAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.DeleteImageBegin, (props: {
    payload: IDeleteImageFormProps;
}) => {
    payload: IDeleteImageFormProps;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.DeleteImageBegin>>;
export declare const DeleteImageSuccessAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.DeleteImageSuccess, (props: {
    response: any;
}) => {
    response: any;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.DeleteImageSuccess>>;
export declare const DeleteImageFailAction: import("@ngrx/store").ActionCreator<WebsiteActionTypes.DeleteImageFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<WebsiteActionTypes.DeleteImageFail>>;
