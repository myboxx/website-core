import { Store } from '@ngrx/store';
import * as fromReducer from './website.reducer';
import { IBusinessFormProps, IDeleteImageFormProps, IWebsiteFormProps } from '../repositories/IWebsite.repository';
export declare class WebsiteStore {
    store: Store<fromReducer.WebsiteState>;
    constructor(store: Store<fromReducer.WebsiteState>);
    get Loading$(): import("rxjs").Observable<boolean>;
    get ImagesLoading$(): import("rxjs").Observable<boolean>;
    get Error$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IWebsiteStateError>;
    get Success$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IWebsiteStateSuccess>;
    LoadWebsite(): void;
    get Website$(): import("rxjs").Observable<import("../models/website.model").WebsitePageModel>;
    get HasBeenFetched$(): import("rxjs").Observable<boolean>;
    UpdateWebsite(siteId: number, payload: IWebsiteFormProps): void;
    UpdateBusinessData(siteId: number, payload: IBusinessFormProps): void;
    deleteImage(payload: IDeleteImageFormProps): void;
}
