import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class WebsiteCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<WebsiteCoreModule>;
}
export {};
