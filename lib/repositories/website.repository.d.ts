import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AbstractAppConfigService, IHttpBasicResponse } from '@boxx/core';
import { IBusinessFormProps, IDeleteImageFormProps, IDeleteImageResponse, IWebsiteApiProps, IWebsiteDataProps, IWebsiteFormProps, IWebsiteRepository } from './IWebsite.repository';
export declare class WebsiteRepository implements IWebsiteRepository {
    private appSettings;
    private httpClient;
    constructor(appSettings: AbstractAppConfigService, httpClient: HttpClient);
    deleteImage(payload: IDeleteImageFormProps): Observable<IHttpBasicResponse<IDeleteImageResponse>>;
    getWebsiteData(): Observable<IHttpBasicResponse<Array<IWebsiteApiProps>>>;
    updateWebsiteData(siteId: number, payload: IWebsiteFormProps): Observable<IHttpBasicResponse<IWebsiteDataProps>>;
    updateBusinessData(siteId: number, payload: IBusinessFormProps): Observable<IHttpBasicResponse<IWebsiteDataProps>>;
    getUploadImagesUrl(): string;
    uploadImage(fileToUpload: File, config: any): Observable<import("@angular/common/http").HttpEvent<IHttpBasicResponse<IWebsiteDataProps>>>;
    getBaseUrl(): string;
}
