import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';
export interface IWebsiteStateError extends IStateErrorBase {
    after: 'GET' | 'UPDATE' | 'UPLOAD_IMAGE' | 'DELETE_IMAGE' | 'UNKNOWN';
}
export interface IWebsiteStateSuccess extends IStateSuccessBase {
    after: 'GET' | 'UPDATE' | 'UPLOAD_IMAGE' | 'DELETE_IMAGE' | 'UNKNOWN';
}
