import { __decorate, __param } from "tslib";
import { HttpEventType } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { catchError, map } from 'rxjs/operators';
import { WebsitePageModel } from '../models/website.model';
import { WEBSITE_REPOSITORY } from '../repositories/IWebsite.repository';
import * as i0 from "@angular/core";
import * as i1 from "../repositories/IWebsite.repository";
var WebsiteService = /** @class */ (function () {
    function WebsiteService(repository) {
        this.repository = repository;
    }
    WebsiteService.prototype.deleteImage = function (payload) {
        return this.repository.deleteImage(payload).pipe(map(function (response) {
            return response.data;
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.getWebsiteData = function () {
        return this.repository.getWebsiteData().pipe(map(function (response) {
            return WebsitePageModel.fromApiResponse(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.updateWebsiteData = function (siteId, payload) {
        return this.repository.updateWebsiteData(siteId, payload).pipe(map(function (response) {
            return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.updateBusinessData = function (siteId, payload) {
        return this.repository.updateBusinessData(siteId, payload).pipe(map(function (response) {
            return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.uploadFileWebBrowser = function (file, params, onProgressEvent) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.repository.uploadImage(file, params)
                .subscribe(function (event) {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        if (onProgressEvent) {
                            onProgressEvent(event);
                        }
                        break;
                    case HttpEventType.Response:
                        resolve(true);
                }
            }, function (error) { return reject(error); });
        });
    };
    WebsiteService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [WEBSITE_REPOSITORY,] }] }
    ]; };
    WebsiteService.ɵprov = i0.ɵɵdefineInjectable({ factory: function WebsiteService_Factory() { return new WebsiteService(i0.ɵɵinject(i1.WEBSITE_REPOSITORY)); }, token: WebsiteService, providedIn: "root" });
    WebsiteService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(WEBSITE_REPOSITORY))
    ], WebsiteService);
    return WebsiteService;
}());
export { WebsiteService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vic2l0ZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvd2Vic2l0ZS1jb3JlLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL3dlYnNpdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRW5ELE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDakQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDM0QsT0FBTyxFQUFvRixrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDOzs7QUFNM0o7SUFDSSx3QkFDd0MsVUFBOEI7UUFBOUIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7SUFDbEUsQ0FBQztJQUNMLG9DQUFXLEdBQVgsVUFBWSxPQUE4QjtRQUN0QyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDNUMsR0FBRyxDQUFDLFVBQUMsUUFBUTtZQUNULE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQztRQUN6QixDQUFDLENBQUMsRUFDRixVQUFVLENBQUMsVUFBQSxLQUFLO1lBQ1osTUFBTSxLQUFLLENBQUM7UUFDaEIsQ0FBQyxDQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7SUFFRCx1Q0FBYyxHQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxDQUFDLElBQUksQ0FDeEMsR0FBRyxDQUFDLFVBQUMsUUFBUTtZQUNULE9BQU8sZ0JBQWdCLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzRCxDQUFDLENBQUMsRUFDRixVQUFVLENBQUMsVUFBQSxLQUFLO1lBQ1osTUFBTSxLQUFLLENBQUM7UUFDaEIsQ0FBQyxDQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7SUFFRCwwQ0FBaUIsR0FBakIsVUFBa0IsTUFBYyxFQUFFLE9BQTBCO1FBQ3hELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUMxRCxHQUFHLENBQUMsVUFBQyxRQUFRO1lBQ1QsT0FBTyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsUUFBUSxFQUFFLEVBQUUsU0FBUyxFQUFFLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDeEcsQ0FBQyxDQUFDLEVBQ0YsVUFBVSxDQUFDLFVBQUEsS0FBSztZQUNaLE1BQU0sS0FBSyxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxDQUNMLENBQUM7SUFDTixDQUFDO0lBRUQsMkNBQWtCLEdBQWxCLFVBQW1CLE1BQWMsRUFBRSxPQUEyQjtRQUMxRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDM0QsR0FBRyxDQUFDLFVBQUMsUUFBUTtZQUNULE9BQU8sZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxFQUFFLFNBQVMsRUFBRSxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3hHLENBQUMsQ0FBQyxFQUNGLFVBQVUsQ0FBQyxVQUFBLEtBQUs7WUFDWixNQUFNLEtBQUssQ0FBQztRQUNoQixDQUFDLENBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztJQUVELDZDQUFvQixHQUFwQixVQUNJLElBQVUsRUFDVixNQUFXLEVBQ1gsZUFBcUM7UUFIekMsaUJBbUJDO1FBZEcsT0FBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLEtBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7aUJBQ3hDLFNBQVMsQ0FBQyxVQUFDLEtBQUs7Z0JBQ2IsUUFBUSxLQUFLLENBQUMsSUFBSSxFQUFFO29CQUNoQixLQUFLLGFBQWEsQ0FBQyxjQUFjO3dCQUM3QixJQUFJLGVBQWUsRUFBRTs0QkFDakIsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUMxQjt3QkFDRCxNQUFNO29CQUNWLEtBQUssYUFBYSxDQUFDLFFBQVE7d0JBQ3ZCLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDckI7WUFDTCxDQUFDLEVBQUUsVUFBQSxLQUFLLElBQUksT0FBQSxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQWIsQ0FBYSxDQUFDLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnREFqRUksTUFBTSxTQUFDLGtCQUFrQjs7O0lBRnJCLGNBQWM7UUFIMUIsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztRQUdPLFdBQUEsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7T0FGdEIsY0FBYyxDQW9FMUI7eUJBL0VEO0NBK0VDLEFBcEVELElBb0VDO1NBcEVZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwRXZlbnRUeXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBXZWJzaXRlUGFnZU1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3dlYnNpdGUubW9kZWwnO1xuaW1wb3J0IHsgSUJ1c2luZXNzRm9ybVByb3BzLCBJRGVsZXRlSW1hZ2VGb3JtUHJvcHMsIElXZWJzaXRlRm9ybVByb3BzLCBJV2Vic2l0ZVJlcG9zaXRvcnksIFdFQlNJVEVfUkVQT1NJVE9SWSB9IGZyb20gJy4uL3JlcG9zaXRvcmllcy9JV2Vic2l0ZS5yZXBvc2l0b3J5JztcbmltcG9ydCB7IElXZWJzaXRlU2VydmljZSB9IGZyb20gJy4vSVdlYnNpdGUuc2VydmljZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgV2Vic2l0ZVNlcnZpY2UgaW1wbGVtZW50cyBJV2Vic2l0ZVNlcnZpY2Uge1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBASW5qZWN0KFdFQlNJVEVfUkVQT1NJVE9SWSkgcHJpdmF0ZSByZXBvc2l0b3J5OiBJV2Vic2l0ZVJlcG9zaXRvcnlcbiAgICApIHsgfVxuICAgIGRlbGV0ZUltYWdlKHBheWxvYWQ6IElEZWxldGVJbWFnZUZvcm1Qcm9wcykge1xuICAgICAgICByZXR1cm4gdGhpcy5yZXBvc2l0b3J5LmRlbGV0ZUltYWdlKHBheWxvYWQpLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIHRocm93IGVycm9yO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXRXZWJzaXRlRGF0YSgpOiBPYnNlcnZhYmxlPFdlYnNpdGVQYWdlTW9kZWw+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMucmVwb3NpdG9yeS5nZXRXZWJzaXRlRGF0YSgpLnBpcGUoXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFdlYnNpdGVQYWdlTW9kZWwuZnJvbUFwaVJlc3BvbnNlKHJlc3BvbnNlLmRhdGEpO1xuICAgICAgICAgICAgfSksXG4gICAgICAgICAgICBjYXRjaEVycm9yKGVycm9yID0+IHtcbiAgICAgICAgICAgICAgICB0aHJvdyBlcnJvcjtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgdXBkYXRlV2Vic2l0ZURhdGEoc2l0ZUlkOiBudW1iZXIsIHBheWxvYWQ6IElXZWJzaXRlRm9ybVByb3BzKTogT2JzZXJ2YWJsZTxXZWJzaXRlUGFnZU1vZGVsPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlcG9zaXRvcnkudXBkYXRlV2Vic2l0ZURhdGEoc2l0ZUlkLCBwYXlsb2FkKS5waXBlKFxuICAgICAgICAgICAgbWFwKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBXZWJzaXRlUGFnZU1vZGVsLmZyb21BcGlSZXNwb25zZShbeyBzaXRlX2lkOiBzaXRlSWQudG9TdHJpbmcoKSwgc2l0ZV9kYXRhOiByZXNwb25zZS5kYXRhIH1dKTtcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgY2F0Y2hFcnJvcihlcnJvciA9PiB7XG4gICAgICAgICAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cblxuICAgIHVwZGF0ZUJ1c2luZXNzRGF0YShzaXRlSWQ6IG51bWJlciwgcGF5bG9hZDogSUJ1c2luZXNzRm9ybVByb3BzKTogT2JzZXJ2YWJsZTxXZWJzaXRlUGFnZU1vZGVsPiB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlcG9zaXRvcnkudXBkYXRlQnVzaW5lc3NEYXRhKHNpdGVJZCwgcGF5bG9hZCkucGlwZShcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gV2Vic2l0ZVBhZ2VNb2RlbC5mcm9tQXBpUmVzcG9uc2UoW3sgc2l0ZV9pZDogc2l0ZUlkLnRvU3RyaW5nKCksIHNpdGVfZGF0YTogcmVzcG9uc2UuZGF0YSB9XSk7XG4gICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoZXJyb3IgPT4ge1xuICAgICAgICAgICAgICAgIHRocm93IGVycm9yO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICB1cGxvYWRGaWxlV2ViQnJvd3NlcihcbiAgICAgICAgZmlsZTogRmlsZSxcbiAgICAgICAgcGFyYW1zOiBhbnksXG4gICAgICAgIG9uUHJvZ3Jlc3NFdmVudDogKGV2ZW50OiBhbnkpID0+IHZvaWRcbiAgICApOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIHRoaXMucmVwb3NpdG9yeS51cGxvYWRJbWFnZShmaWxlLCBwYXJhbXMpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChldmVudCkgPT4ge1xuICAgICAgICAgICAgICAgIHN3aXRjaCAoZXZlbnQudHlwZSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIEh0dHBFdmVudFR5cGUuVXBsb2FkUHJvZ3Jlc3M6XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAob25Qcm9ncmVzc0V2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb25Qcm9ncmVzc0V2ZW50KGV2ZW50KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIEh0dHBFdmVudFR5cGUuUmVzcG9uc2U6XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIGVycm9yID0+IHJlamVjdChlcnJvcikpO1xuICAgICAgICB9KTtcbiAgICB9XG59XG4iXX0=