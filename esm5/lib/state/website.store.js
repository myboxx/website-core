import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSelector from './website.selectors';
import * as fromActions from './website.actions';
var WebsiteStore = /** @class */ (function () {
    function WebsiteStore(store) {
        this.store = store;
    }
    Object.defineProperty(WebsiteStore.prototype, "Loading$", {
        get: function () { return this.store.select(fromSelector.getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "ImagesLoading$", {
        get: function () { return this.store.select(fromSelector.getIsLoadingImages); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "Error$", {
        get: function () { return this.store.select(fromSelector.getError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "Success$", {
        get: function () { return this.store.select(fromSelector.getSuccess); },
        enumerable: true,
        configurable: true
    });
    WebsiteStore.prototype.LoadWebsite = function () { this.store.dispatch(fromActions.LoadWebsiteBeginAction()); };
    Object.defineProperty(WebsiteStore.prototype, "Website$", {
        get: function () { return this.store.select(fromSelector.getWebsiteData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(fromSelector.hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    WebsiteStore.prototype.UpdateWebsite = function (siteId, payload) {
        this.store.dispatch(fromActions.UpdateWebsiteBeginAction({ siteId: siteId, payload: payload }));
    };
    WebsiteStore.prototype.UpdateBusinessData = function (siteId, payload) {
        this.store.dispatch(fromActions.UpdateBusinessBeginAction({ siteId: siteId, payload: payload }));
    };
    WebsiteStore.prototype.deleteImage = function (payload) {
        this.store.dispatch(fromActions.DeleteImageBeginAction({ payload: payload }));
    };
    WebsiteStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    WebsiteStore = __decorate([
        Injectable()
    ], WebsiteStore);
    return WebsiteStore;
}());
export { WebsiteStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vic2l0ZS5zdG9yZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3dlYnNpdGUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9zdGF0ZS93ZWJzaXRlLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxLQUFLLFlBQVksTUFBTSxxQkFBcUIsQ0FBQztBQUNwRCxPQUFPLEtBQUssV0FBVyxNQUFNLG1CQUFtQixDQUFDO0FBTWpEO0lBQ0ksc0JBQW1CLEtBQXNDO1FBQXRDLFVBQUssR0FBTCxLQUFLLENBQWlDO0lBQUksQ0FBQztJQUU5RCxzQkFBSSxrQ0FBUTthQUFaLGNBQWlCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQzs7O09BQUE7SUFFdkUsc0JBQUksd0NBQWM7YUFBbEIsY0FBdUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRW5GLHNCQUFJLGdDQUFNO2FBQVYsY0FBZSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUM7OztPQUFBO0lBRWpFLHNCQUFJLGtDQUFRO2FBQVosY0FBaUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUVyRSxrQ0FBVyxHQUFYLGNBQWdCLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRTVFLHNCQUFJLGtDQUFRO2FBQVosY0FBaUIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDOzs7T0FBQTtJQUV6RSxzQkFBSSx5Q0FBZTthQUFuQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQzFELENBQUM7OztPQUFBO0lBRUQsb0NBQWEsR0FBYixVQUFjLE1BQWMsRUFBRSxPQUEwQjtRQUNwRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsd0JBQXdCLENBQUMsRUFBRSxNQUFNLFFBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNuRixDQUFDO0lBRUQseUNBQWtCLEdBQWxCLFVBQW1CLE1BQWMsRUFBRSxPQUEyQjtRQUMxRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsRUFBRSxNQUFNLFFBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDO0lBRUQsa0NBQVcsR0FBWCxVQUFZLE9BQThCO1FBQ3RDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLE9BQU8sU0FBQSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7O2dCQTVCeUIsS0FBSzs7SUFEdEIsWUFBWTtRQUR4QixVQUFVLEVBQUU7T0FDQSxZQUFZLENBOEJ4QjtJQUFELG1CQUFDO0NBQUEsQUE5QkQsSUE4QkM7U0E5QlksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN0b3JlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0ICogYXMgZnJvbVNlbGVjdG9yIGZyb20gJy4vd2Vic2l0ZS5zZWxlY3RvcnMnO1xuaW1wb3J0ICogYXMgZnJvbUFjdGlvbnMgZnJvbSAnLi93ZWJzaXRlLmFjdGlvbnMnO1xuaW1wb3J0ICogYXMgZnJvbVJlZHVjZXIgZnJvbSAnLi93ZWJzaXRlLnJlZHVjZXInO1xuaW1wb3J0IHsgSUJ1c2luZXNzRm9ybVByb3BzLCBJRGVsZXRlSW1hZ2VGb3JtUHJvcHMsIElXZWJzaXRlRm9ybVByb3BzIH0gZnJvbSAnLi4vcmVwb3NpdG9yaWVzL0lXZWJzaXRlLnJlcG9zaXRvcnknO1xuXG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBXZWJzaXRlU3RvcmUge1xuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBzdG9yZTogU3RvcmU8ZnJvbVJlZHVjZXIuV2Vic2l0ZVN0YXRlPikgeyB9XG5cbiAgICBnZXQgTG9hZGluZyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0SXNMb2FkaW5nKTsgfVxuXG4gICAgZ2V0IEltYWdlc0xvYWRpbmckKCkgeyByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldElzTG9hZGluZ0ltYWdlcyk7IH1cblxuICAgIGdldCBFcnJvciQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0RXJyb3IpOyB9XG5cbiAgICBnZXQgU3VjY2VzcyQoKSB7IHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0U3VjY2Vzcyk7IH1cblxuICAgIExvYWRXZWJzaXRlKCkgeyB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkxvYWRXZWJzaXRlQmVnaW5BY3Rpb24oKSk7IH1cblxuICAgIGdldCBXZWJzaXRlJCgpIHsgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRXZWJzaXRlRGF0YSk7IH1cblxuICAgIGdldCBIYXNCZWVuRmV0Y2hlZCQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuaGFzQmVlbkZldGNoZWQpO1xuICAgIH1cblxuICAgIFVwZGF0ZVdlYnNpdGUoc2l0ZUlkOiBudW1iZXIsIHBheWxvYWQ6IElXZWJzaXRlRm9ybVByb3BzKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuVXBkYXRlV2Vic2l0ZUJlZ2luQWN0aW9uKHsgc2l0ZUlkLCBwYXlsb2FkIH0pKTtcbiAgICB9XG5cbiAgICBVcGRhdGVCdXNpbmVzc0RhdGEoc2l0ZUlkOiBudW1iZXIsIHBheWxvYWQ6IElCdXNpbmVzc0Zvcm1Qcm9wcykge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLlVwZGF0ZUJ1c2luZXNzQmVnaW5BY3Rpb24oeyBzaXRlSWQsIHBheWxvYWQgfSkpO1xuICAgIH1cblxuICAgIGRlbGV0ZUltYWdlKHBheWxvYWQ6IElEZWxldGVJbWFnZUZvcm1Qcm9wcykge1xuICAgICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkRlbGV0ZUltYWdlQmVnaW5BY3Rpb24oeyBwYXlsb2FkIH0pKTtcbiAgICB9XG59XG4iXX0=