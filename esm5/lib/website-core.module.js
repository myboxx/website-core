import { __decorate, __read, __spread } from "tslib";
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { WEBSITE_REPOSITORY } from './repositories/IWebsite.repository';
import { WebsiteRepository } from './repositories/website.repository';
import { WEBSITE_SERVICE } from './services/IWebsite.service';
import { WebsiteService } from './services/website.service';
import { WebsiteEffects } from './state/website.effects';
import { websiteReducer } from './state/website.reducer';
import { WebsiteStore } from './state/website.store';
var WebsiteCoreModule = /** @class */ (function () {
    function WebsiteCoreModule() {
    }
    WebsiteCoreModule_1 = WebsiteCoreModule;
    WebsiteCoreModule.forRoot = function (config) {
        return {
            ngModule: WebsiteCoreModule_1,
            providers: __spread([
                { provide: WEBSITE_SERVICE, useClass: WebsiteService },
                { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository }
            ], config.providers, [
                WebsiteStore
            ])
        };
    };
    var WebsiteCoreModule_1;
    WebsiteCoreModule = WebsiteCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('website', websiteReducer),
                EffectsModule.forFeature([WebsiteEffects]),
            ],
            exports: []
        })
    ], WebsiteCoreModule);
    return WebsiteCoreModule;
}());
export { WebsiteCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vic2l0ZS1jb3JlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3dlYnNpdGUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi93ZWJzaXRlLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQXVCLFFBQVEsRUFBWSxNQUFNLGVBQWUsQ0FBQztBQUN4RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDeEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDdEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzlELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQWVyRDtJQUFBO0lBWUEsQ0FBQzswQkFaWSxpQkFBaUI7SUFDbkIseUJBQU8sR0FBZCxVQUFlLE1BQThCO1FBQ3pDLE9BQU87WUFDTCxRQUFRLEVBQUUsbUJBQWlCO1lBQzNCLFNBQVM7Z0JBQ1AsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxjQUFjLEVBQUU7Z0JBQ3RELEVBQUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBRTtlQUN6RCxNQUFNLENBQUMsU0FBUztnQkFDbkIsWUFBWTtjQUNiO1NBQ0YsQ0FBQztJQUNKLENBQUM7O0lBWE0saUJBQWlCO1FBVDdCLFFBQVEsQ0FBQztZQUNSLFlBQVksRUFBRSxFQUFFO1lBQ2hCLE9BQU8sRUFBRTtnQkFDUCxnQkFBZ0I7Z0JBQ2hCLFdBQVcsQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQztnQkFDakQsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQzNDO1lBQ0QsT0FBTyxFQUFFLEVBQUU7U0FDWixDQUFDO09BQ1csaUJBQWlCLENBWTdCO0lBQUQsd0JBQUM7Q0FBQSxBQVpELElBWUM7U0FaWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIFByb3ZpZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFZmZlY3RzTW9kdWxlIH0gZnJvbSAnQG5ncngvZWZmZWN0cyc7XG5pbXBvcnQgeyBTdG9yZU1vZHVsZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IFdFQlNJVEVfUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lXZWJzaXRlLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgV2Vic2l0ZVJlcG9zaXRvcnkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy93ZWJzaXRlLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgV0VCU0lURV9TRVJWSUNFIH0gZnJvbSAnLi9zZXJ2aWNlcy9JV2Vic2l0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IFdlYnNpdGVTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy93ZWJzaXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgV2Vic2l0ZUVmZmVjdHMgfSBmcm9tICcuL3N0YXRlL3dlYnNpdGUuZWZmZWN0cyc7XG5pbXBvcnQgeyB3ZWJzaXRlUmVkdWNlciB9IGZyb20gJy4vc3RhdGUvd2Vic2l0ZS5yZWR1Y2VyJztcbmltcG9ydCB7IFdlYnNpdGVTdG9yZSB9IGZyb20gJy4vc3RhdGUvd2Vic2l0ZS5zdG9yZSc7XG5cbmludGVyZmFjZSBNb2R1bGVPcHRpb25zSW50ZXJmYWNlIHtcbiAgICBwcm92aWRlcnM6IFByb3ZpZGVyW107XG59XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW10sXG4gIGltcG9ydHM6IFtcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgIFN0b3JlTW9kdWxlLmZvckZlYXR1cmUoJ3dlYnNpdGUnLCB3ZWJzaXRlUmVkdWNlciksXG4gICAgRWZmZWN0c01vZHVsZS5mb3JGZWF0dXJlKFtXZWJzaXRlRWZmZWN0c10pLFxuICBdLFxuICBleHBvcnRzOiBbXVxufSlcbmV4cG9ydCBjbGFzcyBXZWJzaXRlQ29yZU1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoY29uZmlnOiBNb2R1bGVPcHRpb25zSW50ZXJmYWNlKTogTW9kdWxlV2l0aFByb3ZpZGVyczxXZWJzaXRlQ29yZU1vZHVsZT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIG5nTW9kdWxlOiBXZWJzaXRlQ29yZU1vZHVsZSxcbiAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgIHsgcHJvdmlkZTogV0VCU0lURV9TRVJWSUNFLCB1c2VDbGFzczogV2Vic2l0ZVNlcnZpY2UgfSxcbiAgICAgICAgICAgIHsgcHJvdmlkZTogV0VCU0lURV9SRVBPU0lUT1JZLCB1c2VDbGFzczogV2Vic2l0ZVJlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgIC4uLmNvbmZpZy5wcm92aWRlcnMsXG4gICAgICAgICAgICBXZWJzaXRlU3RvcmVcbiAgICAgICAgICBdXG4gICAgICAgIH07XG4gICAgICB9XG59XG4iXX0=