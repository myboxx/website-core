import { __decorate, __param, __assign, __spread } from 'tslib';
import { HttpParams, HttpClient, HttpEventType, HttpClientModule } from '@angular/common/http';
import { InjectionToken, Inject, Injectable, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

var WEBSITE_REPOSITORY = new InjectionToken('websiteRepository');

var WebsiteRepository = /** @class */ (function () {
    function WebsiteRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    WebsiteRepository.prototype.deleteImage = function (payload /*website_id: string, container: string, index: string, url_img: string*/) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/delete_image", body);
    };
    WebsiteRepository.prototype.getWebsiteData = function () {
        return this.httpClient.get(this.getBaseUrl() + "/sites");
    };
    WebsiteRepository.prototype.updateWebsiteData = function (siteId, payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update_site/" + siteId, body);
    };
    WebsiteRepository.prototype.updateBusinessData = function (siteId, payload) {
        var urlSearchParams = new URLSearchParams();
        Object.keys(payload).forEach(function (key) {
            (typeof payload[key] === 'object') ?
                urlSearchParams.append(key, JSON.stringify(payload[key]))
                :
                    urlSearchParams.append(key, payload[key]);
        });
        var body = urlSearchParams.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update_business_info/" + siteId, body);
    };
    WebsiteRepository.prototype.getUploadImagesUrl = function () {
        return this.getBaseUrl() + '/upload_update_images';
    };
    WebsiteRepository.prototype.uploadImage = function (fileToUpload, config) {
        var formData = new FormData();
        formData.append('img', fileToUpload, fileToUpload.name.replace('jpeg', 'jpg'));
        Object.keys(config).forEach(function (key) {
            formData.append(key, config[key]);
        });
        return this.httpClient.post("" + this.getUploadImagesUrl(), formData, {
            reportProgress: true,
            observe: 'events'
        });
    };
    WebsiteRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/website";
    };
    WebsiteRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    WebsiteRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], WebsiteRepository);
    return WebsiteRepository;
}());

var WEBSITE_SERVICE = new InjectionToken('websiteService');

var WebsitePageModel = /** @class */ (function () {
    function WebsitePageModel(data) {
        this.websiteList = data.websiteList;
    }
    WebsitePageModel.fromApiResponse = function (raw) {
        var websiteList = raw.map(function (resp) {
            return WebsiteModel.fromApiResponse(resp);
        });
        return new WebsitePageModel({
            websiteList: websiteList
        });
    };
    WebsitePageModel.empty = function () {
        return new WebsitePageModel({
            websiteList: [WebsiteModel.empty()]
        });
    };
    return WebsitePageModel;
}());
var WebsiteModel = /** @class */ (function () {
    function WebsiteModel(data) {
        this.siteId = data.siteId;
        this.title = data.title;
        this.email = data.email;
        this.facebook = data.facebook && data.facebook !== 'null' ? data.facebook : '';
        this.twitter = data.twitter && data.twitter !== 'null' ? data.twitter : '';
        this.instagram = data.instagram && data.instagram !== 'null' ? data.instagram : '';
        this.gplus = data.gplus && data.gplus !== 'null' ? data.gplus : '';
        this.siteUrl = data.siteUrl;
        this.subdomain = data.subdomain;
        this.about = data.about;
        this.displayPicture = data.displayPicture;
        this.phone = data.phone;
        this.mobile = data.mobile;
        this.hoursOfOperation = data.hoursOfOperation || [];
        this.hoursOfOperationNotes = data.hoursOfOperationNotes || '';
        this.paymentForms = data.paymentForms || [];
        this.products = data.products || [];
    }
    WebsiteModel.fromApiResponse = function (data) {
        var siteUrl = (data.site_data.site_url || '').replace('https', 'http');
        if (siteUrl) {
            siteUrl = siteUrl.startsWith('http') ? siteUrl : 'http://' + siteUrl;
        }
        return new WebsiteModel({
            siteId: +data.site_id,
            title: data.site_data.title,
            email: data.site_data.email,
            facebook: data.site_data.facebook,
            twitter: data.site_data.twitter,
            instagram: data.site_data.instagram,
            gplus: data.site_data.gplus,
            siteUrl: siteUrl,
            subdomain: data.site_data.subdomian,
            about: data.site_data.about,
            displayPicture: {
                carrouselUrlImages: data.site_data.display_picture.carrousel_url_images,
                logoUrlImage: data.site_data.display_picture.logo_url_image,
                bannerUrl: data.site_data.tagline
            },
            phone: data.site_data.phone,
            mobile: data.site_data.mobile,
            hoursOfOperation: data.site_data.hours_of_operation,
            hoursOfOperationNotes: data.site_data.hours_of_operation_notes,
            paymentForms: data.site_data.payment_forms,
            products: data.site_data.products
        });
    };
    WebsiteModel.empty = function () {
        return new WebsiteModel({
            siteId: null,
            title: null,
            email: null,
            facebook: null,
            twitter: null,
            instagram: null,
            gplus: null,
            siteUrl: null,
            subdomain: null,
            about: null,
            displayPicture: null,
            phone: null,
            mobile: null,
            hoursOfOperation: null,
            hoursOfOperationNotes: null,
            paymentForms: null,
            products: null
        });
    };
    return WebsiteModel;
}());

var WebsiteService = /** @class */ (function () {
    function WebsiteService(repository) {
        this.repository = repository;
    }
    WebsiteService.prototype.deleteImage = function (payload) {
        return this.repository.deleteImage(payload).pipe(map(function (response) {
            return response.data;
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.getWebsiteData = function () {
        return this.repository.getWebsiteData().pipe(map(function (response) {
            return WebsitePageModel.fromApiResponse(response.data);
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.updateWebsiteData = function (siteId, payload) {
        return this.repository.updateWebsiteData(siteId, payload).pipe(map(function (response) {
            return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.updateBusinessData = function (siteId, payload) {
        return this.repository.updateBusinessData(siteId, payload).pipe(map(function (response) {
            return WebsitePageModel.fromApiResponse([{ site_id: siteId.toString(), site_data: response.data }]);
        }), catchError(function (error) {
            throw error;
        }));
    };
    WebsiteService.prototype.uploadFileWebBrowser = function (file, params, onProgressEvent) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.repository.uploadImage(file, params)
                .subscribe(function (event) {
                switch (event.type) {
                    case HttpEventType.UploadProgress:
                        if (onProgressEvent) {
                            onProgressEvent(event);
                        }
                        break;
                    case HttpEventType.Response:
                        resolve(true);
                }
            }, function (error) { return reject(error); });
        });
    };
    WebsiteService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [WEBSITE_REPOSITORY,] }] }
    ]; };
    WebsiteService.ɵprov = ɵɵdefineInjectable({ factory: function WebsiteService_Factory() { return new WebsiteService(ɵɵinject(WEBSITE_REPOSITORY)); }, token: WebsiteService, providedIn: "root" });
    WebsiteService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(WEBSITE_REPOSITORY))
    ], WebsiteService);
    return WebsiteService;
}());

var WebsiteActionTypes;
(function (WebsiteActionTypes) {
    WebsiteActionTypes["LoadWebsiteBegin"] = "[WEBSITE] Load Webiste Begin";
    WebsiteActionTypes["LoadWebsiteSuccess"] = "[WEBSITE] Load Webiste Success";
    WebsiteActionTypes["LoadWebsiteFail"] = "[WEBSITE] Load Webiste Fail";
    WebsiteActionTypes["UpdateWebsiteBegin"] = "[WEBSITE] Update Webiste Begin";
    WebsiteActionTypes["UpdateWebsiteSuccess"] = "[WEBSITE] Update Webiste Success";
    WebsiteActionTypes["UpdateWebsiteFail"] = "[WEBSITE] Update Webiste Fail";
    WebsiteActionTypes["UpdateBusinessBegin"] = "[WEBSITE] Update Business Begin";
    WebsiteActionTypes["UpdateBusinessSuccess"] = "[WEBSITE] Update Business Success";
    WebsiteActionTypes["UpdateBusinessFail"] = "[WEBSITE] Update Business Fail";
    WebsiteActionTypes["DeleteImageBegin"] = "[WEBSITE] Delete Image Begin";
    WebsiteActionTypes["DeleteImageSuccess"] = "[WEBSITE] Delete Image Success";
    WebsiteActionTypes["DeleteImageFail"] = "[WEBSITE] Delete Image Fail";
})(WebsiteActionTypes || (WebsiteActionTypes = {}));
var LoadWebsiteBeginAction = createAction(WebsiteActionTypes.LoadWebsiteBegin);
var LoadWebsiteSuccessAction = createAction(WebsiteActionTypes.LoadWebsiteSuccess, props());
var LoadWebsiteFailAction = createAction(WebsiteActionTypes.LoadWebsiteFail, props());
var UpdateWebsiteBeginAction = createAction(WebsiteActionTypes.UpdateWebsiteBegin, props());
var UpdateWebsiteSuccessAction = createAction(WebsiteActionTypes.UpdateWebsiteSuccess, props());
var UpdateWebsiteFailAction = createAction(WebsiteActionTypes.UpdateWebsiteFail, props());
var UpdateBusinessBeginAction = createAction(WebsiteActionTypes.UpdateBusinessBegin, props());
var UpdateBusinessSuccessAction = createAction(WebsiteActionTypes.UpdateBusinessSuccess, props());
var UpdateBusinessFailAction = createAction(WebsiteActionTypes.UpdateBusinessFail, props());
// -------------------- Image section -----------------
var DeleteImageBeginAction = createAction(WebsiteActionTypes.DeleteImageBegin, props());
var DeleteImageSuccessAction = createAction(WebsiteActionTypes.DeleteImageSuccess, props());
var DeleteImageFailAction = createAction(WebsiteActionTypes.DeleteImageFail, props());

var WebsiteEffects = /** @class */ (function () {
    function WebsiteEffects(actions$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.service = service;
        this.loadWebsite$ = createEffect(function () { return _this.actions$.pipe(ofType(WebsiteActionTypes.LoadWebsiteBegin), switchMap(function () {
            return _this.service.getWebsiteData().pipe(map(function (websitePage) { return LoadWebsiteSuccessAction({ payload: websitePage }); }), catchError(function (errors) {
                console.error('WebsiteEffects.loadWebsite$ ERROR', errors);
                return of(LoadWebsiteFailAction({ errors: errors }));
            }));
        })); });
        this.updateWebsite$ = createEffect(function () { return _this.actions$.pipe(ofType(WebsiteActionTypes.UpdateWebsiteBegin), switchMap(function (action) {
            return _this.service.updateWebsiteData(action.siteId, action.payload).pipe(map(function (response) {
                return UpdateWebsiteSuccessAction({ payload: response });
            }), catchError(function (errors) {
                console.error('WebsiteEffects.updateWebsite$ ERROR', errors);
                return of(UpdateWebsiteFailAction({ errors: errors }));
            }));
        })); });
        this.updateBusiness$ = createEffect(function () { return _this.actions$.pipe(ofType(WebsiteActionTypes.UpdateBusinessBegin), switchMap(function (action) {
            return _this.service.updateBusinessData(action.siteId, action.payload).pipe(map(function (response) {
                return UpdateBusinessSuccessAction({ payload: response });
            }), catchError(function (errors) {
                console.error('WebsiteEffects.updateBusiness$ ERROR', errors);
                return of(UpdateBusinessFailAction({ errors: errors }));
            }));
        })); });
        this.deleteImage$ = createEffect(function () { return _this.actions$.pipe(ofType(WebsiteActionTypes.DeleteImageBegin), switchMap(function (action) {
            return _this.service.deleteImage(action.payload).pipe(map(function (response) {
                return DeleteImageSuccessAction({ response: response });
            }), catchError(function (errors) {
                console.error('WebsiteEffects.deleteImage$ ERROR', errors);
                return of(DeleteImageFailAction({ errors: errors }));
            }));
        })); });
    }
    WebsiteEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: undefined, decorators: [{ type: Inject, args: [WEBSITE_SERVICE,] }] }
    ]; };
    WebsiteEffects = __decorate([
        Injectable(),
        __param(1, Inject(WEBSITE_SERVICE))
    ], WebsiteEffects);
    return WebsiteEffects;
}());

var ɵ0 = WebsitePageModel.empty();
var initialState = {
    isLoading: false,
    isLoadingImages: false,
    data: ɵ0,
    hasBeenFetched: false,
    error: null,
    success: null
};
var ɵ1 = function (state, action) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: action.type !== WebsiteActionTypes.DeleteImageBegin ? true : false, isLoadingImages: action.type === WebsiteActionTypes.DeleteImageBegin ? true : false })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, isLoadingImages: false, error: { after: getErrorActionType(action.type), error: action.errors } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, hasBeenFetched: true, data: action.payload, success: { after: getSuccessActionType(action.type) } })); }, ɵ4 = function (state, action) { return (__assign(__assign({}, state), { isLoadingImages: false, success: { after: getSuccessActionType(action.type) } })); }, ɵ5 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, data: action.payload, success: { after: getSuccessActionType(action.type) } })); };
var reducer = createReducer(initialState, 
// LOAD SECTION
on(LoadWebsiteBeginAction, UpdateWebsiteBeginAction, UpdateBusinessBeginAction, DeleteImageBeginAction, ɵ1), on(LoadWebsiteFailAction, UpdateWebsiteFailAction, UpdateBusinessFailAction, DeleteImageFailAction, ɵ2), on(LoadWebsiteSuccessAction, ɵ3), on(DeleteImageSuccessAction, ɵ4), on(UpdateWebsiteSuccessAction, UpdateBusinessSuccessAction, ɵ5));
function getErrorActionType(type) {
    var action;
    switch (type) {
        case WebsiteActionTypes.LoadWebsiteFail:
            action = 'GET';
            break;
        case WebsiteActionTypes.UpdateWebsiteFail:
        case WebsiteActionTypes.UpdateBusinessFail:
            action = 'UPDATE';
            break;
        case WebsiteActionTypes.DeleteImageFail:
            action = 'DELETE_IMAGE';
            break;
    }
    return action;
}
function getSuccessActionType(type) {
    var action;
    switch (type) {
        case WebsiteActionTypes.LoadWebsiteSuccess:
            action = 'GET';
            break;
        case WebsiteActionTypes.UpdateWebsiteSuccess:
        case WebsiteActionTypes.UpdateBusinessSuccess:
            action = 'UPDATE';
            break;
        case WebsiteActionTypes.DeleteImageSuccess:
            action = 'DELETE_IMAGE';
            break;
    }
    return action;
}
function websiteReducer(state, action) {
    return reducer(state, action);
}

var getWebsiteState = createFeatureSelector('website');
var ɵ0$1 = function (state) { return state; };
var getWebsitePageState = createSelector(getWebsiteState, ɵ0$1);
var stateGetIsLoading = function (state) { return state.isLoading; };
var stateGetIsLoadingImages = function (state) { return state.isLoadingImages; };
var stateGetWebsiteData = function (state) { return state.data; };
var getIsLoading = createSelector(getWebsitePageState, stateGetIsLoading);
var getIsLoadingImages = createSelector(getWebsitePageState, stateGetIsLoadingImages);
var ɵ1$1 = function (state) { return state.error; };
var getError = createSelector(getWebsitePageState, ɵ1$1);
var ɵ2$1 = function (state) { return state.success; };
var getSuccess = createSelector(getWebsitePageState, ɵ2$1);
var getWebsiteData = createSelector(getWebsitePageState, stateGetWebsiteData);
var ɵ3$1 = function (state) { return state.hasBeenFetched; };
var hasBeenFetched = createSelector(getWebsitePageState, ɵ3$1);

var WebsiteStore = /** @class */ (function () {
    function WebsiteStore(store) {
        this.store = store;
    }
    Object.defineProperty(WebsiteStore.prototype, "Loading$", {
        get: function () { return this.store.select(getIsLoading); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "ImagesLoading$", {
        get: function () { return this.store.select(getIsLoadingImages); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "Error$", {
        get: function () { return this.store.select(getError); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "Success$", {
        get: function () { return this.store.select(getSuccess); },
        enumerable: true,
        configurable: true
    });
    WebsiteStore.prototype.LoadWebsite = function () { this.store.dispatch(LoadWebsiteBeginAction()); };
    Object.defineProperty(WebsiteStore.prototype, "Website$", {
        get: function () { return this.store.select(getWebsiteData); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WebsiteStore.prototype, "HasBeenFetched$", {
        get: function () {
            return this.store.select(hasBeenFetched);
        },
        enumerable: true,
        configurable: true
    });
    WebsiteStore.prototype.UpdateWebsite = function (siteId, payload) {
        this.store.dispatch(UpdateWebsiteBeginAction({ siteId: siteId, payload: payload }));
    };
    WebsiteStore.prototype.UpdateBusinessData = function (siteId, payload) {
        this.store.dispatch(UpdateBusinessBeginAction({ siteId: siteId, payload: payload }));
    };
    WebsiteStore.prototype.deleteImage = function (payload) {
        this.store.dispatch(DeleteImageBeginAction({ payload: payload }));
    };
    WebsiteStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    WebsiteStore = __decorate([
        Injectable()
    ], WebsiteStore);
    return WebsiteStore;
}());

var WebsiteCoreModule = /** @class */ (function () {
    function WebsiteCoreModule() {
    }
    WebsiteCoreModule_1 = WebsiteCoreModule;
    WebsiteCoreModule.forRoot = function (config) {
        return {
            ngModule: WebsiteCoreModule_1,
            providers: __spread([
                { provide: WEBSITE_SERVICE, useClass: WebsiteService },
                { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository }
            ], config.providers, [
                WebsiteStore
            ])
        };
    };
    var WebsiteCoreModule_1;
    WebsiteCoreModule = WebsiteCoreModule_1 = __decorate([
        NgModule({
            declarations: [],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('website', websiteReducer),
                EffectsModule.forFeature([WebsiteEffects]),
            ],
            exports: []
        })
    ], WebsiteCoreModule);
    return WebsiteCoreModule;
}());

/*
 * Public API Surface of website-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DeleteImageBeginAction, DeleteImageFailAction, DeleteImageSuccessAction, LoadWebsiteBeginAction, LoadWebsiteFailAction, LoadWebsiteSuccessAction, UpdateBusinessBeginAction, UpdateBusinessFailAction, UpdateBusinessSuccessAction, UpdateWebsiteBeginAction, UpdateWebsiteFailAction, UpdateWebsiteSuccessAction, WEBSITE_REPOSITORY, WEBSITE_SERVICE, WebsiteActionTypes, WebsiteCoreModule, WebsiteEffects, WebsiteModel, WebsitePageModel, WebsiteRepository, WebsiteService, WebsiteStore, getError, getIsLoading, getIsLoadingImages, getSuccess, getWebsiteData, getWebsitePageState, getWebsiteState, hasBeenFetched, initialState, stateGetIsLoading, stateGetIsLoadingImages, stateGetWebsiteData, websiteReducer, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3 };
//# sourceMappingURL=boxx-website-core.js.map
