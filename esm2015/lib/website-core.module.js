var WebsiteCoreModule_1;
import { __decorate } from "tslib";
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { WEBSITE_REPOSITORY } from './repositories/IWebsite.repository';
import { WebsiteRepository } from './repositories/website.repository';
import { WEBSITE_SERVICE } from './services/IWebsite.service';
import { WebsiteService } from './services/website.service';
import { WebsiteEffects } from './state/website.effects';
import { websiteReducer } from './state/website.reducer';
import { WebsiteStore } from './state/website.store';
let WebsiteCoreModule = WebsiteCoreModule_1 = class WebsiteCoreModule {
    static forRoot(config) {
        return {
            ngModule: WebsiteCoreModule_1,
            providers: [
                { provide: WEBSITE_SERVICE, useClass: WebsiteService },
                { provide: WEBSITE_REPOSITORY, useClass: WebsiteRepository },
                ...config.providers,
                WebsiteStore
            ]
        };
    }
};
WebsiteCoreModule = WebsiteCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('website', websiteReducer),
            EffectsModule.forFeature([WebsiteEffects]),
        ],
        exports: []
    })
], WebsiteCoreModule);
export { WebsiteCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2Vic2l0ZS1jb3JlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0Bib3h4L3dlYnNpdGUtY29yZS8iLCJzb3VyY2VzIjpbImxpYi93ZWJzaXRlLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFlckQsSUFBYSxpQkFBaUIseUJBQTlCLE1BQWEsaUJBQWlCO0lBQzFCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBOEI7UUFDekMsT0FBTztZQUNMLFFBQVEsRUFBRSxtQkFBaUI7WUFDM0IsU0FBUyxFQUFFO2dCQUNULEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsY0FBYyxFQUFFO2dCQUN0RCxFQUFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7Z0JBQzVELEdBQUcsTUFBTSxDQUFDLFNBQVM7Z0JBQ25CLFlBQVk7YUFDYjtTQUNGLENBQUM7SUFDSixDQUFDO0NBQ04sQ0FBQTtBQVpZLGlCQUFpQjtJQVQ3QixRQUFRLENBQUM7UUFDUixZQUFZLEVBQUUsRUFBRTtRQUNoQixPQUFPLEVBQUU7WUFDUCxnQkFBZ0I7WUFDaEIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDO1lBQ2pELGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUMzQztRQUNELE9BQU8sRUFBRSxFQUFFO0tBQ1osQ0FBQztHQUNXLGlCQUFpQixDQVk3QjtTQVpZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XG5pbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSwgUHJvdmlkZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgV0VCU0lURV9SRVBPU0lUT1JZIH0gZnJvbSAnLi9yZXBvc2l0b3JpZXMvSVdlYnNpdGUucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBXZWJzaXRlUmVwb3NpdG9yeSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL3dlYnNpdGUucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBXRUJTSVRFX1NFUlZJQ0UgfSBmcm9tICcuL3NlcnZpY2VzL0lXZWJzaXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgV2Vic2l0ZVNlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL3dlYnNpdGUuc2VydmljZSc7XG5pbXBvcnQgeyBXZWJzaXRlRWZmZWN0cyB9IGZyb20gJy4vc3RhdGUvd2Vic2l0ZS5lZmZlY3RzJztcbmltcG9ydCB7IHdlYnNpdGVSZWR1Y2VyIH0gZnJvbSAnLi9zdGF0ZS93ZWJzaXRlLnJlZHVjZXInO1xuaW1wb3J0IHsgV2Vic2l0ZVN0b3JlIH0gZnJvbSAnLi9zdGF0ZS93ZWJzaXRlLnN0b3JlJztcblxuaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXSxcbiAgaW1wb3J0czogW1xuICAgIEh0dHBDbGllbnRNb2R1bGUsXG4gICAgU3RvcmVNb2R1bGUuZm9yRmVhdHVyZSgnd2Vic2l0ZScsIHdlYnNpdGVSZWR1Y2VyKSxcbiAgICBFZmZlY3RzTW9kdWxlLmZvckZlYXR1cmUoW1dlYnNpdGVFZmZlY3RzXSksXG4gIF0sXG4gIGV4cG9ydHM6IFtdXG59KVxuZXhwb3J0IGNsYXNzIFdlYnNpdGVDb3JlTW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChjb25maWc6IE1vZHVsZU9wdGlvbnNJbnRlcmZhY2UpOiBNb2R1bGVXaXRoUHJvdmlkZXJzPFdlYnNpdGVDb3JlTW9kdWxlPiB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgbmdNb2R1bGU6IFdlYnNpdGVDb3JlTW9kdWxlLFxuICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgeyBwcm92aWRlOiBXRUJTSVRFX1NFUlZJQ0UsIHVzZUNsYXNzOiBXZWJzaXRlU2VydmljZSB9LFxuICAgICAgICAgICAgeyBwcm92aWRlOiBXRUJTSVRFX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBXZWJzaXRlUmVwb3NpdG9yeSB9LFxuICAgICAgICAgICAgLi4uY29uZmlnLnByb3ZpZGVycyxcbiAgICAgICAgICAgIFdlYnNpdGVTdG9yZVxuICAgICAgICAgIF1cbiAgICAgICAgfTtcbiAgICAgIH1cbn1cbiJdfQ==